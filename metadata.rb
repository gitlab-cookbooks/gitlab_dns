name 'gitlab_dns'
maintainer 'MIT'
maintainer_email 'john@gitlab.com'
license 'all_rights'
description 'Installs/Configures gitlab_dns'
long_description 'Installs/Configures gitlab_dns'
issues_url 'https://gitlab.com/gitlab-cookbooks/gitlab_dns/issues' if respond_to?(:issues_url)
source_url 'https://gitlab.com/gitlab-cookbooks/gitlab_dns' if respond_to?(:source_url)
version '0.1.7'

