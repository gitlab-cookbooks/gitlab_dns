#
# Cookbook:: build_cookbook
# Recipe:: lint
#
# Copyright:: 2017, MIT, All Rights Reserved.
include_recipe 'delivery-truck::lint'
