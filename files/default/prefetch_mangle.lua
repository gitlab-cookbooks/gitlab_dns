function preresolve(dq)
        local parsedCname
        local section
        section = ""
        query = dq.qname:toString()

        pdnslog("Incoming query: "..query.." from "..dq.remoteaddr:toString().." to "..dq.localaddr:toString())

        if query:match("sv") or query:match("lb") or query:match("db") query:match("inf") then
                pdnslog(">>>>>>> Heeeey. I matched!")
                parsedCname, subs = string.gsub(query, "%.%w+%.%w+%.gitlab%.com%.", ".node.consul.")
        else
                pdnslog(">>>>>>> Doing nuttin' for:" .. query);
                return false;
        end
        pdnslog(subs)
        if subs == 1 then
                pdnslog(">>>>>>> parsedCname = " .. parsedCname);
                dq:addAnswer(pdns.CNAME, parsedCname)
                dq.rcode = 0
                dq.followupFunction="followCNAMERecords"    -- this makes PowerDNS lookup your CNAME                
                return true
        else
                pdnslog(">>>>>>> No subs found for " .. query);
        end

        -- we do not know how to resolve, let it continue recursing
        return false;

end