# Perform the needful for PowerDNS Recursive Servers
#

apt_package 'pdns-recursor' do
    action :upgrade
end

cookbook_file '/etc/powerdns/prefetch_mangle.lua' do
    source 'prefetch_mangle.lua'
end

template "/etc/powerdns/recursor.conf" do
    source 'recursor.conf.erb'
    owner 'root'
    group 'root'
    mode 0644
end
